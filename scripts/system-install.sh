cd ${WWW}

if ! [ -x "$(command -v git)" ]; then
  echo "Git not installed"
  exit;
fi

# Downloading files into bitbucket repository
git clone https://bitbucket.org/hfocus/maestro . &&\
git pull origin production

mv ${HOME}/maestro-default/* ${WWW}

# Install system dependencies
echo "Install system dependencies"
sudo LC_ALL=C.UTF-8 add-apt-repository -y ppa:ondrej/php;
sudo apt-get update;
sudo apt-get install php5.6-dev php-pear make;
sudo pecl install mongodb-1.7.4;
sudo su;
echo "extension=mongodb.so" >> /etc/php/5.6/fpm/php.ini;
echo "extension=mongodb.so" >> /etc/php/5.6/cli/php.ini;
echo "extension=mongodb.so" >> /etc/php/5.6/apache2/php.ini;
exit;

# Install Composer PHP dependencies
echo "Install Composer PHP dependencies"
sh ${HOME}/scripts/composer-install.sh

# Configure VHost
echo "Configure VHost"
sudo rm -rf /etc/nginx/sites-avaliable/default;
sudo rm -rf /etc/nginx/sites-enabled/default;
sudo service nginx restart;