FROM ubuntu:16.04
MAINTAINER HFocus

ENV USER=maestro
ENV HOME=/home/$USER
ENV WWW=/var/www/$USER

# Make USER sudo group
RUN apt-get update && \
    apt-get install sudo && \
    adduser $USER --disabled-password --gecos GECOS && \
    echo "$USER ALL=(root) NOPASSWD:ALL" > /etc/sudoers.d/$USER && \
    chmod 0440 /etc/sudoers.d/$USER

RUN su - $USER -c "touch dockerfile-hfocus1.0"

# Workdir default
WORKDIR $HOME

# ENVIRONMENT VARIABLES
ENV DEBIAN_FRONTEND noninteractive
ENV ENVIRONMENT homolog

RUN DEBIAN_FRONTEND=noninteractive \
    apt-get update && \
    apt-get install -y language-pack-en-base &&\
    export LC_ALL=en_US.UTF-8 && \
    export LANG=en_US.UTF-8

RUN DEBIAN_FRONTEND=noninteractive apt-get update && apt-get install -y software-properties-common
RUN DEBIAN_FRONTEND=noninteractive LC_ALL=en_US.UTF-8 add-apt-repository ppa:ondrej/php

RUN DEBIAN_FRONTEND=noninteractive LC_ALL=en_US.UTF-8

# SYSTEM UTILS
RUN apt-get update
RUN apt-get install python-software-properties -y
RUN apt-get install curl -y

# Importing scripts
ADD ./scripts/ $HOME/scripts
RUN chmod +x `ls $HOME/scripts/*.sh`

# Installing NGINX
RUN apt-get install nginx -y
RUN echo '<h1>Receita Maestro Instalada com sucesso!</h1>' > /var/www/html/index.html
COPY nginx/nginx.conf /etc/nginx/nginx.conf
COPY nginx/conf.d/php-fpm.conf /etc/nginx/conf.d/php-fpm.conf
COPY nginx/sites-available/maestro /etc/nginx/sites-available/maestro
RUN ln -s /etc/nginx/sites-available/maestro /etc/nginx/sites-enabled/maestro

#Setting www public html
RUN mkdir $WWW
RUN chmod a+rwx /var/www
ADD ./maestro-default/ $HOME/maestro-default/
RUN chown $USER:www-data /var/www -R

# Installing GIT
RUN add-apt-repository ppa:git-core/ppa -y
RUN apt-get update
RUN apt-get install git -y

# Installing PHP && Libraries
RUN apt-get purge `dpkg -l | grep php| awk '{print $2}' |tr "\n" " "`
RUN apt-get install php5.6 -y
RUN apt-get install php5.6-mbstring php5.6-mcrypt php5.6-mysql php5.6-xml php5.6-zip php5.6-json php5.6-curl php5.6-phalcon php-mongodb php5.6-mongo php5.6-fpm -y
COPY php-fpm/php.ini /etc/php/5.6/fpm/php.ini
COPY php-fpm/www.conf /etc/php/5.6/fpm/pool.d/www.conf

# Removing Apache2
RUN apt-get purge apache2 -y
RUN apt-get autoremove -y
RUN rm -rf /etc/apache2

# File editors
RUN apt-get install nano vim -y

# Updating
RUN apt-get update

EXPOSE 22
EXPOSE 80
EXPOSE 433
EXPOSE 9000

# BASH
ENTRYPOINT sudo ./scripts/http-start.sh && /bin/bash
CMD ["su", "-", "$USER", "-c", "/usr/sbin/init", "/bin/bash"]